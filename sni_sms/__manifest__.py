# -*- coding: utf-8 -*-

{
    'name': 'SNI School Management System',
    'version': '1.0.0',
    'category': 'School Management System',
    'author': 'SNI',
    'summary': 'SNI School Management System',
    'sequence': 10,
    'description': """
    SNI School Management System
    """,
    'depends': [
        'sni_core',
        'sni_accounting',
        'sni_attendance',
        'sni_rfid',
    ],
    'data': [
        # security
        'security/sni_sms_security.xml',
        'security/ir.model.access.csv',

        # wizard
        'wizard/batch_invoice_kelas_wizard.xml',

        # views
        'views/sni_orang_views.xml',
        'views/sni_kelas_views.xml',
        'views/sni_rfid_views.xml',
        'views/sni_attendance_views.xml',
        'views/sni_jurusan_views.xml',
        'views/sni_orangtua_views.xml',
        
        'views/sni_sms_menuitem.xml',
    ],
    'application': False
}