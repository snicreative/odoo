# -*- coding: utf-8 -*-

from odoo import models, fields

class BatchInvoiceKelas(models.TransientModel):
    _name = 'sni.batch.invoice.kelas.wizard'
    _description = 'Batch Invoice Kelas'

    kelas_id = fields.Many2one('sni.kelas', 'Kelas', required=True)

    nama_order = fields.Char('Nama Order', required=True)
    note = fields.Text('Keterangan')

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id
    
    date_order = fields.Datetime(string='Order Date', required=True, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    jumlah = fields.Monetary('Jumlah', required=True)

    def create_batch_invoice(self):
        batch_invoice_id = self.env['sni.batch.invoice'].create({
            'nama_order': self.nama_order,
            'origin': self.kelas_id.name,
            'note': self.note,
            'orang_ids': [(6,0,self.kelas_id.siswa_ids.ids)],
            'jumlah': self.jumlah,
        })

        batch_invoice_id.action_done()

        return True
        
        view_id = self.env.ref('sni_accounting.view_sni_batch_invoice_form').id
        
        return {'type': 'ir.actions.act_window',
                'name': _('Batch Invoice Kelas'),
                'res_model': 'sni.batch.invoice.kelas.wizard',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }