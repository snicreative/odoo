# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Orang(models.Model):
    _inherit = 'sni.orang'

    tipe_orang = fields.Selection([
        ('siswa', 'Siswa'),
        ('guru', 'Guru'),
    ], 'Tipe', default='siswa')

    kelas_id = fields.Many2one('sni.kelas', 'Rombel Saat Ini', tracking=True)
    rfid_ids = fields.One2many('sni.rfid', 'orang_id', 'RFID')

    nis = fields.Char('NIS', tracking=True)
    nisn = fields.Char('NISN', tracking=True)
    
    nomor_identitas = fields.Char('Nomor Identitas', compute="_compute_nomor_identitas", store=True)

    @api.depends('tipe_orang', 'nik', 'nis')
    def _compute_nomor_identitas(self):
        for orang in self:
            if orang.tipe_orang == 'siswa':
                orang.nomor_identitas = orang.nis
            else:
                orang.nomor_identitas = orang.nik

    def name_get(self):
        return [(orang.id, '[{}] {}'.format(orang.nomor_identitas, orang.name)) for orang in self]

    orangtua_id = fields.Many2one('sni.orangtua', 'Orang Tua')

    def _to_json(self):
        res = super(Orang, self)._to_json()

        res['tipe_orang'] = self.tipe_orang
        res['nis'] = self.nis
        res['nisn'] = self.nisn

        if self.orangtua_id:
            res['orangtua_id'] = self.orangtua_id._to_json()
        
        return res