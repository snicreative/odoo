# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class OrangTua(models.Model):
    _name = 'sni.orangtua'
    _description = 'Orang Tua'

    name = fields.Char('Nama', required=True)

    no_whatsapp = fields.Char('No Whatsapp')

    def send_whatsapp(self, message):
        message = self.env['sni.whatsapp'].sudo().create({
            'no_tlp': self.no_whatsapp,
            'pesan': message,
            'status': 0,
            'tgl_kirim': fields.Datetime.now()
        })
        return message
        
    def _to_json(self):
        self.ensure_one()

        res = {
            'name': self.name,
            'no_whatsapp': self.no_whatsapp
        }

        return res

    siswa_ids = fields.One2many('sni.orang', 'orangtua_id', 'Siswa')

    