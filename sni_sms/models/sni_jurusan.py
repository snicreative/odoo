# -*- coodng: utd-8 -*-

from odoo import models, fields, api, _

class Jurusan(models.Model):
    _name = 'sni.jurusan'
    _description = "Jurusan"

    name = fields.Char('Nama Jurusan', required=True)
    