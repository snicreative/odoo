# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Kelas(models.Model):
    _name = 'sni.kelas'
    _description = 'Kelas'

    name = fields.Char('Nama Kelas', required=True)

    jurusan_id = fields.Many2one('sni.jurusan', 'Jurusan')

    siswa_ids = fields.One2many('sni.orang', 'kelas_id', 'Siswa', domain=[('tipe_orang', '=', 'siswa')])
    siswa_count = fields.Integer('# Siswa',
        compute='_compute_siswa_count', compute_sudo=False)

    def _compute_siswa_count(self):
        for kelas in self:
            kelas.siswa_count = self.env['sni.orang'].search_count([('tipe_orang', '=', 'siswa'), ('kelas_id', '=', kelas.id)])

    guru_id = fields.Many2one('sni.orang', 'Wali Kelas', domain=[('tipe_orang', '=', 'guru')])

    def action_view_siswa(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('sni_sms.action_sni_orang_siswa')
        siswa = self.mapped('siswa_ids')

        if len(siswa) > 1:
            action['domain'] = [('id', 'in', siswa.ids)]

        elif siswa:
            form_view = [(self.env.ref('sni_core.view_sni_orang_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = siswa.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

    def action_batch_invoice_kelas(self):
        view_id = self.env.ref('sni_sms.batch_invoice_kelas_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Batch Invoice Kelas'),
                'res_model': 'sni.batch.invoice.kelas.wizard',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
                'context': {'default_kelas_id': self.id} 
        }