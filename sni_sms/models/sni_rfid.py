# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Rfid(models.Model):
    _inherit = 'sni.rfid'

    orang_id = fields.Many2one('sni.orang', 'Orang', tracking=True)
    nomor_identitas = fields.Char('NIS', related="orang_id.nomor_identitas", store=True)

    def _to_json(self):
        res = super(Rfid, self)._to_json()
        if self.orang_id:
            res['orang_id'] = self.orang_id._to_json()
        
        return res