# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Attendance(models.Model):
    _inherit = 'sni.attendance'

    orang_id = fields.Many2one('sni.orang', related='rfid_id.orang_id', store=True)

    nomor_identitas = fields.Char('NIS', related="orang_id.nomor_identitas", store=True)

    kelas_id = fields.Many2one('sni.kelas', related="orang_id.kelas_id", store=True)