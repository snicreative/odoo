# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Guestbook(models.Model):
    _name = 'sni.guestbook'
    _description = 'Guestbook'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'timestamp desc'

    name = fields.Char(
        'Reference', copy=False, readonly=True, default=lambda x: _('New'))

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            values['name'] = self.env['ir.sequence'].next_by_code('sni.guestbook') or _('New')
        guestbook = super(Guestbook, self).create(values)

        guestbook.orang_id.send_whatsapp(guestbook.keperluan)

        return guestbook

    timestamp = fields.Datetime(string='Timestamp', required=True, readonly=True, index=True, tracking=True, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")

    nama_lengkap = fields.Char('Nama Lengkap', required=True)
    nama_instansi = fields.Char('Nama Instansi')
    alamat_instansi = fields.Text('Alamat Instansi')

    kontak = fields.Char('No Handphone / Whatsapp', required=True)

    orang_id = fields.Many2one('sni.orang', 'Person Tujuan')

    jumlah_orang = fields.Integer('Jumlah Orang', default=1.0)

    keperluan = fields.Text('Keperluan')