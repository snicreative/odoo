# -*- coding: utf-8 -*-

{
    'name': 'SNI Guestbook',
    'version': '1.0.0',
    'category': 'Guestbook',
    'author': 'SNI',
    'summary': 'Guestbook Module',
    'sequence': 10,
    'description': """
    Guestbook
    """,
    'depends': [
        'sni_core'
    ],
    'data': [
        # data
        'data/ir_sequence_data.xml',

        # security
        'security/sni_guestbook_security.xml',
        'security/ir.model.access.csv',

        # views
        'views/sni_guestbook_views.xml',

        'views/sni_guestbook_menuitem.xml',
    ],
    'application': False
}