# -*- coding: utf-8 -*-

{
    'name': 'SNI RFID',
    'version': '1.0.0',
    'category': 'RFID',
    'author': 'SNI',
    'summary': 'RFID Module',
    'sequence': 10,
    'description': """
    RFID Integration
    """,
    'depends': [
        'sni_core'
    ],
    'data': [
        # security
        'security/sni_rfid_security.xml',
        'security/ir.model.access.csv',

        # views
        'views/sni_rfid_views.xml',
        'views/sni_station_views.xml',

        'views/sni_rfid_menuitem.xml',
    ],
    'application': False
}