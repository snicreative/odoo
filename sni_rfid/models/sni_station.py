# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Station(models.Model):
    _name = 'sni.station'
    _description = 'Station RFID'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    active = fields.Boolean('Active', default=True, store=True, readonly=False)
    
    name = fields.Char(related='identifier')

    identifier = fields.Char('ID', required=True)
    
    _sql_constraints = [
        ('identifier_uniq', 'unique (identifier)', 'The identifier must be unique !')
    ]

    is_verified = fields.Boolean(default=False)
    date_verified = fields.Datetime('Date Verified', default=False, tracking=True)

    def action_verify(self):
        self.ensure_one()
        
        self.date_verified = fields.Datetime.now()
        self.is_verified = True

        return True

    def action_unverify(self):
        self.ensure_one()
        
        self.date_verified = False
        self.is_verified = False

        return True