# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Rfid(models.Model):
    _name = 'sni.rfid'
    _description = 'RFID Key'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    active = fields.Boolean('Active', default=True, store=True, readonly=False)

    name = fields.Char(related='key')
    key = fields.Char('RFID Key', required=True)
    type = fields.Selection([
        ('card', 'Card'),
        ('tag', 'Tag'),
        ('other', 'Other')
    ], 'RFID Type', default='card', required=True)

    _sql_constraints = [
        ('key_uniq', 'unique (key)', 'The key must be unique !')
    ]

    def _to_json(self):
        self.ensure_one()

        return {
            'key': self.key,
            'type': self.type,
            'date_verified': self.date_verified.strftime('%Y-%m-%d %H:%M:%S')
        }

    is_verified = fields.Boolean(default=False)
    date_verified = fields.Datetime('Date Verified', default=False, tracking=True)

    def action_verify(self):
        self.ensure_one()
        
        self.date_verified = fields.Datetime.now()
        self.is_verified = True

        return True

    def action_unverify(self):
        self.ensure_one()
        
        self.date_verified = False
        self.is_verified = False

        return True