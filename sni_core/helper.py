# -*- coding: utf-8 -*-
from json import dumps
from odoo.http import Response, request

import logging

_logger = logging.getLogger(__name__)

def responseJSON(status, message, data, status_code, addtl_headers=None, debug=False, secretkey_id=False):
    data = {'status': status, 'message': message, 'data': data}

    url = request.httprequest.url
    remote_addr = request.httprequest.remote_addr
    method = request.httprequest.method
    form = request.httprequest.form.to_dict()

    _logger.info("[{} {}] {} : {}".format(method, status.upper(), url, data))
    
    request.env['sni.request.log'].sudo().add_log({
        'url': url,
        'remote_addr': remote_addr,
        'method': method,
        'request_body': form,
        'response_body': data,
    })

    if debug:
        return data

    headers = {'Content-Type': 'application/json'}
    if addtl_headers and isinstance(addtl_headers, dict):
        headers = {**headers, **addtl_headers}
    return Response(dumps(data), status=status_code, headers=headers)