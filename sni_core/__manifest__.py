# -*- coding: utf-8 -*-

{
    'name': 'SNI Core',
    'version': '1.0.0',
    'category': 'SNI',
    'author': 'SNI',
    'summary': 'SNI Core',
    'sequence': 10,
    'description': """
    SNI Core
    """,
    'depends': [
        'base_setup',
        'mail'
    ],
    'data': [
        # security
        'security/sni_security.xml',
        'security/ir.model.access.csv',

        # views
        'views/sni_orang_views.xml',
        'views/request_log_views.xml',
        'views/sni_whatsapp_views.xml',

        'views/sni_menuitem.xml',
    ],
    'application': False
}