# -*- coding: utf-8 -*-

from odoo import models, api, fields, _

class Whatsapp(models.Model):
    _name = 'sni.whatsapp'
    _description = 'Whatsapp'

    no = fields.Integer(related='id', store=True)
    no_tlp = fields.Char('No Telepon', required=True)
    pesan = fields.Text()
    status = fields.Integer('Status', default=0)
    tgl_kirim = fields.Datetime('Tanggal Kirim')

    def send_message(self, no_tlp, pesan):
        self.env['sni.whatsapp'].create({
            'no_tlp': no_tlp,
            'pesan': pesan
        })

        return True