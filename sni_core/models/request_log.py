# -*- coding: utf-8 -*-

from odoo import models, fields

class RequestLog(models.Model):
    _name = 'sni.request.log'
    _description = 'Request Log'
    _order = 'timestamp desc'

    url = fields.Char('URL', required=True)
    remote_addr = fields.Char('Remote Address', required=True)
    method = fields.Char()
    request_body = fields.Text('Form Data')
    response_body = fields.Text('Response')
    timestamp = fields.Datetime(default=fields.Datetime.now)

    def name_get(self):
        return [[log.id, '[{}] {}'.format(log.method, log.url)] for log in self]

    '''
        Utils
    '''

    def add_log(self, data):
        self.create(data)
        return True
