# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Orang(models.Model):
    _name = 'sni.orang'
    _description = 'Orang'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'image.mixin']

    active = fields.Boolean('Active', default=True, store=True, readonly=False)
    name = fields.Char('Nama', required=True)

    tempat_tanggal_lahir = fields.Char('Tempat Tanggal Lahir')

    nik = fields.Char('NIK', required=True, tracking=True)
    jenis_kelamin = fields.Selection([
        ('L', 'Laki-laki'),
        ('P', 'Perempuan')
    ], string='Jenis Kelamin')
    agama = fields.Char('Agama')

    alamat = fields.Text('Alamat')
    alamat_rt = fields.Char('RT')
    alamat_rw = fields.Char('RW')
    alamat_kelurahan = fields.Char('Kelurahan')
    alamat_kecamatan = fields.Char('Kecamatan')
    golongan_darah = fields.Char('Golongan Darah')

    no_telepon = fields.Char('No Telepon')
    no_whatsapp = fields.Char('No Whatsapp')

    def send_whatsapp(self, message):
        message = self.env['sni.whatsapp'].sudo().create({
            'no_tlp': self.no_whatsapp,
            'pesan': message,
            'status': 0,
            'tgl_kirim': fields.Datetime.now()
        })
        return message
        
    def _to_json(self):
        res = {
            # 'id': self.id,
            'name': self.name,
            'tempat_tanggal_lahir': self.tempat_tanggal_lahir,
            'nik': self.nik,
            'jenis_kelamin': self.jenis_kelamin,
            'agama': self.agama,
            'alamat': self.alamat,
            'alamat_rt': self.alamat_rt,
            'alamat_rw': self.alamat_rw,
            'alamat_kelurahan': self.alamat_kelurahan,
            'alamat_kecamatan': self.alamat_kecamatan,
            'golongan_darah': self.golongan_darah,
            'no_telepon': self.no_telepon,
            'no_whatsapp': self.no_whatsapp,
        }
        return res