# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Order(models.Model):
    _name = 'sni.order'
    _description = 'Order'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date_order desc'

    name = fields.Char(
        'Reference', copy=False, readonly=True, default=lambda x: _('New'))

    batch_invoice_id = fields.Many2one('sni.batch.invoice', 'Batch Invoice')

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            if values['order_type'] == 'invoice':
                values['name'] = self.env['ir.sequence'].next_by_code('sni.order.invoice') or _('New')
            elif values['order_type'] == 'bill':
                values['name'] = self.env['ir.sequence'].next_by_code('sni.order.bill') or _('New')
        order = super(Order, self).create(values)
        return order

    nama_order = fields.Char('Nama Order', required=True)

    order_type = fields.Selection([
        ('invoice', 'Invoice'),
        ('bill', 'Bill')
    ], 'Order Type', default="invoice", required=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft')
    orang_id = fields.Many2one('sni.orang', 'Orang', readonly=True, states={'draft': [('readonly', False)]})
    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True, tracking=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")
    origin = fields.Char(string='Source Document', help="Reference of the document that generated this order request.", tracking=True)
    note = fields.Text('Keterangan', readonly=True, states={'draft': [('readonly', False)]})

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    jumlah = fields.Monetary('Jumlah', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)

    jumlah_terbayar = fields.Monetary('Jumlah Terbayar', compute="_compute_jumlah", store=True)

    sisa_bayar = fields.Monetary('Sisa Bayar', compute="_compute_jumlah", store=True, tracking=True)

    pembayaran_ids = fields.One2many('sni.pembayaran', 'order_id', 'Pembayaran')

    pembayaran_count = fields.Integer('# Pembayaran', compute="_compute_jumlah")

    @api.depends('pembayaran_ids', 'pembayaran_ids.jumlah', 'pembayaran_ids.state')
    def _compute_jumlah(self):
        for order in self:
            if order.pembayaran_ids:
                order.jumlah_terbayar = sum(order.pembayaran_ids.filtered(lambda x: x.state == 'done').mapped('jumlah'))
                order.sisa_bayar = order.jumlah - order.jumlah_terbayar
                order.pembayaran_count = len(order.pembayaran_ids)

                if order.sisa_bayar == 0:
                    order.state = 'done'
            else:
                order.jumlah_terbayar = 0
                order.sisa_bayar = order.jumlah
                order.pembayaran_count = 0
    
    def action_done(self):
        self.ensure_one()

        self.state = 'done'

        return True

    def action_bayar(self):
        view_id = self.env.ref('sni_accounting.order_bayar_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Pembayaran Order'),
                'res_model': 'sni.order.bayar.wizard',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
                'context': {'default_order_id': self.id, 'default_jumlah': self.sisa_bayar} 
        }

    def action_confirm(self):
        self.ensure_one()

        if self.jumlah <= 0:
            raise ValidationError(_('Jumlah must greater than zero.'))

        self.state = 'confirmed'

        return True

    def action_cancel(self):
        self.ensure_one()

        self.state = 'cancel'

        return True

    def action_view_pembayaran(self):
        self.ensure_one()
        
        action = self.env["ir.actions.actions"]._for_xml_id("sni_accounting.action_sni_pembayaran_in")
        if self.order_type == 'bill':
            action = self.env["ir.actions.actions"]._for_xml_id("sni_accounting.action_sni_pembayaran_out")

        pembayaran = self.mapped('pembayaran_ids')

        if len(pembayaran) > 1:
            action['domain'] = [('id', 'in', pembayaran.ids)]

        elif pembayaran:
            form_view = [(self.env.ref('sni_accounting.view_sni_pembayaran_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = pembayaran.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action