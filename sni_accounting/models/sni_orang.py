# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Orang(models.Model):
    _inherit = 'sni.orang'

    order_ids = fields.One2many('sni.order', 'orang_id', 'Order')

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    total_order = fields.Monetary(compute='_compute_order', string="Total order")
    sisa_hutang = fields.Monetary(compute='_compute_order', string="Sisa Hutang")

    @api.depends('order_ids', 'order_ids.state', 'order_ids.jumlah')
    def _compute_order(self):
        for orang in self:
            if orang.order_ids:
                orang.total_order = sum(orang.order_ids.filtered(lambda x: x.state not in ['cancel']).mapped('jumlah'))
                orang.sisa_hutang = sum(orang.order_ids.filtered(lambda x: x.state not in ['draft', 'cancel']).mapped('sisa_bayar'))
            else:
                orang.total_order = 0
                orang.sisa_hutang = 0
    
    def action_view_order_orang(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("sni_accounting.action_sni_order_invoice")
        order = self.mapped('order_ids')

        if len(order) > 1:
            action['domain'] = [('id', 'in', order.ids)]

        elif order:
            form_view = [(self.env.ref('sni_accounting.view_sni_order_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = order.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

    def action_view_order_hutang_orang(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("sni_accounting.action_sni_order_invoice")
        order = self.mapped('order_ids')

        if len(order) > 1:
            action['domain'] = [
                ('id', 'in', order.ids),
                ('sisa_bayar', '>', 0),
            ]

        elif order:
            form_view = [(self.env.ref('sni_accounting.view_sni_order_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']
            else:
                action['views'] = form_view

            action['res_id'] = order.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action