# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Pembayaran(models.Model):
    _name = 'sni.pembayaran'
    _description = 'Pembayaran'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date_paid desc'

    name = fields.Char(
        'Reference', copy=False, readonly=True, default=lambda x: _('New'))

    orang_id = fields.Many2one('sni.orang', 'Orang')
    
    origin = fields.Char('Source Document')

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            if values['tipe_pembayaran'] == 'in':
                values['name'] = self.env['ir.sequence'].next_by_code('sni.pembayaran.in') or _('New')
            elif values['tipe_pembayaran'] == 'out':
                values['name'] = self.env['ir.sequence'].next_by_code('sni.pembayaran.out') or _('New')
        pembayaran = super(Pembayaran, self).create(values)
        return pembayaran

    date_paid = fields.Datetime(string='Date Paid', required=True, readonly=True, index=True, tracking=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")
    kas_id = fields.Many2one('sni.kas', 'Kas', required=True)

    tipe_pembayaran = fields.Selection([
        ('in', 'In'),
        ('out', 'Out')
    ], 'Tipe Pembayaran', required=True, default='in')

    order_id = fields.Many2one('sni.order', 'order', required=True, readonly=True, states={'draft': [('readonly', False)]})

    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Paid'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft')

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    jumlah = fields.Monetary('Jumlah', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)

    def action_done(self):
        self.ensure_one()

        if self.jumlah > self.order_id.sisa_bayar:
            raise ValidationError(_('Overpayment!'))

        self.state = 'done'

        return True

    def action_cancel(self):
        self.ensure_one()

        self.state = 'cancel'

        return True

    