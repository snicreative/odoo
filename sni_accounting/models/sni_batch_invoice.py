# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions

class BatchInvoice(models.Model):
    _name = 'sni.batch.invoice'
    _description = "Batch Invoice"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date_order desc'

    name = fields.Char(
        'Reference', copy=False, readonly=True, default=lambda x: _('New'))

    nama_order = fields.Char('Nama Order', required=True)

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            values['name'] = self.env['ir.sequence'].next_by_code('sni.batch.invoice') or _('New')
        order = super(BatchInvoice, self).create(values)
        return order

    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Validated'),
        ('cancel', 'Canceled')
    ], default='draft', tracking=True)

    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True, tracking=True, states={'draft': [('readonly', False)]}, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")
    origin = fields.Char(string='Source Document', help="Reference of the document that generated this order request.", tracking=True)
    note = fields.Text('Keterangan', readonly=True, states={'draft': [('readonly', False)]})

    orang_ids = fields.Many2many('sni.orang', string="Orang", required=True, tracking=True)

    order_ids = fields.One2many('sni.order', 'batch_invoice_id', 'Order')

    order_count = fields.Integer('# Invoice',
        compute='_compute_order_count', compute_sudo=False)

    def _compute_order_count(self):
        for batch_invoice in self:
            batch_invoice.order_count = self.env['sni.order'].search_count([('batch_invoice_id', '=', batch_invoice.id)])

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    jumlah = fields.Monetary('Jumlah', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)

    def action_done(self):
        self.ensure_one()

        if not self.orang_ids:
            raise exceptions.ValidationError(_("You must add orang at least one."))

        if self.jumlah <= 0:
            raise exceptions.ValidationError(_("Jumlah must bigger then zero."))

        self.write({
            'state': 'done'
        })

        for orang_id in self.orang_ids:
            self.env['sni.order'].create({
                'nama_order': self.nama_order,
                'order_type': 'invoice',
                'orang_id': orang_id.id,
                'date_order': self.date_order,
                'origin': "{}{}".format(self.name, " - {}".format(self.origin) if self.origin else ""),
                'note': self.note,
                'jumlah': self.jumlah,
                'batch_invoice_id': self.id
            })

        return True

    def action_cancel(self):
        self.ensure_one()

        self.write({
            'state': 'cancel'
        })

        return True

    def action_view_order(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("sni_accounting.action_sni_order_invoice")
        order = self.mapped('order_ids')

        if len(order) > 1:
            action['domain'] = [('id', 'in', order.ids)]

        elif order:
            form_view = [(self.env.ref('sni_accounting.view_sni_order_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = order.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action