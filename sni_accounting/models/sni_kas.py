# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Kas(models.Model):
    _name = 'sni.kas'
    _description = 'Kas'

    kas_type = fields.Selection([
        ('bank', 'Bank'),
        ('cash', 'Cash')
    ], string='Tipe Kas', default='cash')

    name = fields.Char('Nama', required=True)

    no_rekening = fields.Char('No Rekening')

    keterangan = fields.Text()

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    pembayaran_ids = fields.One2many('sni.pembayaran', 'kas_id', 'Pembayaran')

    in_pembayaran_ids = fields.One2many('sni.pembayaran', 'kas_id', compute='_compute_saldo')
    out_pembayaran_ids = fields.One2many('sni.pembayaran', 'kas_id', compute='_compute_saldo')

    count_pembayaran_in = fields.Integer('# Pembayaran In', compute="_compute_saldo", store=True)
    count_pembayaran_out = fields.Integer('# Pembayaran Out', compute="_compute_saldo", store=True)

    total_pendapatan = fields.Float('Pendapatan', compute="_compute_saldo", store=True)
    total_pengeluaran = fields.Float('Pengeluaran', compute="_compute_saldo", store=True)

    saldo = fields.Float('Balance', compute="_compute_saldo", store=True)

    @api.depends('pembayaran_ids')
    def _compute_saldo(self):
        for kas in self:
            kas.in_pembayaran_ids = kas.pembayaran_ids.filtered(lambda x: x.tipe_pembayaran == 'in')
            kas.out_pembayaran_ids = kas.pembayaran_ids.filtered(lambda x: x.tipe_pembayaran == 'out')

            kas.count_pembayaran_in = len(kas.in_pembayaran_ids)
            kas.count_pembayaran_out = len(kas.out_pembayaran_ids)

            kas.total_pendapatan = sum(kas.pembayaran_ids.filtered(lambda x: x.tipe_pembayaran == 'in').mapped('jumlah'))
            kas.total_pengeluaran = sum(kas.pembayaran_ids.filtered(lambda x: x.tipe_pembayaran == 'out').mapped('jumlah'))

            kas.saldo = kas.total_pendapatan - kas.total_pengeluaran

    def action_view_pembayaran_in(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('sni_accounting.action_sni_pembayaran_in')
        pembayaran = self.mapped('in_pembayaran_ids')

        if len(pembayaran) > 1:
            action['domain'] = [('id', 'in', pembayaran.ids)]

        elif pembayaran:
            form_view = [(self.env.ref('sni_accounting.view_sni_pembayaran_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = pembayaran.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

    def action_view_pembayaran_out(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('sni_accounting.action_sni_pembayaran_out')
        pembayaran = self.mapped('out_pembayaran_ids')

        if len(pembayaran) > 1:
            action['domain'] = [('id', 'in', pembayaran.ids)]

        elif pembayaran:
            form_view = [(self.env.ref('sni_accounting.view_sni_pembayaran_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = pembayaran.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action