# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class OrderBayar(models.TransientModel):
    _name = 'sni.order.bayar.wizard'
    _description = 'Pembayaran order'

    order_id = fields.Many2one('sni.order', 'order', required=True)

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    jumlah = fields.Monetary('Jumlah Bayar')
    kas_id = fields.Many2one('sni.kas', 'Kas', required=True)

    def bayar(self):
        pembayaran_id = self.env['sni.pembayaran'].create({
            'order_id': self.order_id.id,
            'orang_id': self.order_id.orang_id.id or False,
            'jumlah': self.jumlah,
            'tipe_pembayaran': 'out' if self.order_id.order_type == 'bill' else 'in',
            'kas_id': self.kas_id.id,
            'origin': self.order_id.name,
        })

        pembayaran_id.action_done()
        return True