# -*- coding: utf-8 -*-

{
    'name': 'SNI Accounting',
    'version': '1.0.0',
    'category': 'SNI',
    'author': 'SNI',
    'summary': 'Accounting',
    'sequence': 10,
    'description': """
    Sistem Penagihan dan Pembayaran
    """,
    'depends': [
        'sni_core'
    ],
    'data': [
        # security
        'security/sni_accounting_security.xml',
        'security/ir.model.access.csv',

        # data
        'data/ir_sequence_data.xml',

        # wizard
        'wizard/order_bayar_wizard.xml',

        # views
        'views/sni_order_views.xml',
        'views/sni_pembayaran_views.xml',
        'views/sni_orang_views.xml',
        'views/sni_batch_invoice_views.xml',
        'views/sni_kas_views.xml',

        'views/sni_accounting_menuitem.xml',
    ],
    'application': False
}