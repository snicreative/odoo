# -*- coding: utf-8 -*-

{
    'name': 'SNI Attendance',
    'version': '1.0.0',
    'category': 'SNI',
    'author': 'SNI',
    'summary': 'Attendance Integrated with RFID',
    'sequence': 10,
    'description': """
    Attendance RFID Integration
    """,
    'depends': [
        'sni_core',
        'sni_rfid',
    ],
    'data': [
        # security
        'security/sni_attendance_security.xml',
        'security/ir.model.access.csv',

        # views
        'views/sni_attendance_views.xml',
        'views/sni_attendance_rule_views.xml',

        'views/sni_attendance_menuitem.xml',
    ],
    'application': False
}