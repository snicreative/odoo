# -*- coding: utf-8 -*-

import datetime, pytz

from odoo.http import Controller, request, route

from odoo.addons.sni_core.helper import responseJSON

import logging

_logger = logging.getLogger(__name__)

class Attendance(Controller):
    '''
        RFID Check In / Check out
    '''
    @route('/api/<string(minlength:1):station_identifier>/<string(minlength:1):rfid_key>/attendance', auth='none', methods=["POST"], sitemap=False, website=False, csrf=False)
    def user_attendance(self, station_identifier, rfid_key, debug=False, **req):
        url = request.httprequest.url
        method = request.httprequest.method
        _logger.info("[{}] to {} : {}". format(method, url, req))

        station_id = request.env['sni.station'].sudo().search([('identifier', '=', station_identifier)], limit=1)

        if not station_id:
            station_id = request.env['sni.station'].sudo().create({
                'identifier': station_identifier
            })
            return responseJSON(status='success', message="New Station Registered. Contact Admin to Verified the Station.", data=[], status_code=200, debug=debug)
        
        if station_id.is_verified == False:
            return responseJSON(status='warning', message="Station Unverified. Contact Admin to Verified the Station.", data=[], status_code=200, debug=debug)

        new_rfid = False
        rfid_id = request.env['sni.rfid'].sudo().search([('key', '=', rfid_key)], limit=1)
        
        if not rfid_id:
            _logger.info("[ATTENDANCE] New RFID Registered")
            rfid_id = request.env['sni.rfid'].sudo().create({
                'key': rfid_key
            })
            new_rfid = True

        res = rfid_id.attendance_action(new_rfid)

        return responseJSON(status=res['status'], message=res['message'], data=res['data'], status_code=200, debug=debug)
        