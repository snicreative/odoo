# -*- coding: utf-8 -*-

import pytz, datetime

from odoo import models, fields, api, _, exceptions

# put POSIX 'Etc/*' entries at the end to avoid confusing users - see bug 1086728
_tzs = [(tz, tz) for tz in sorted(pytz.all_timezones, key=lambda tz: tz if not tz.startswith('Etc/') else '_')]
def _tz_get(self):
    return _tzs

import logging

_logger = logging.getLogger(__name__)

def float_time(time):
    return '{0:02.0f}:{1:02.0f}'.format(*divmod(time * 60, 60))

class AttendanceRule(models.Model):
    _name = 'sni.attendance.rule'
    _description = 'Attendance Rule'

    name = fields.Char(compute="_compute_name")

    @api.depends('start_time', 'end_time', 'attendance_type')
    def _compute_name(self):
        for rule in self:
            rule.name = "[{}] {} - {}".format(str.upper(rule.attendance_type), float_time(rule.start_time), float_time(rule.end_time))

    start_time = fields.Float('Start Time (24H)', required=True)
    end_time = fields.Float('End Time (24H)', required=True)

    start_time_utc = fields.Float('Start Time UTC', compute="_compute_time_utc", store=True)
    end_time_utc = fields.Float('End Time UTC', compute="_compute_time_utc", store=True)

    @api.depends('start_time', 'end_time', 'tz')
    def _compute_time_utc(self):
        for rule in self:
            user_tz = pytz.timezone(rule.tz or pytz.utc)
            _logger.info(user_tz)
            start_datetime = datetime.datetime.strptime("{} {}".format(datetime.datetime.now().strftime('%Y-%m-%d'), float_time(rule.start_time)), "%Y-%m-%d %H:%M")
            end_datetime = datetime.datetime.strptime("{} {}".format(datetime.datetime.now().strftime('%Y-%m-%d'), float_time(rule.end_time)), "%Y-%m-%d %H:%M")
            start_utc_datetime = user_tz.localize(start_datetime).astimezone(pytz.utc)
            end_utc_datetime = user_tz.localize(end_datetime).astimezone(pytz.utc)
            st = start_utc_datetime.time()
            et = end_utc_datetime.time()
            rule.start_time_utc = st.hour+st.minute/60.0
            rule.end_time_utc = et.hour+et.minute/60.0

    tz = fields.Selection(_tz_get, string='Timezone', default=lambda self: self._context.get('tz'),
                          help="When printing documents and exporting/importing data, time values are computed according to this timezone.\n"
                               "If the timezone is not set, UTC (Coordinated Universal Time) is used.\n"
                               "Anywhere else, time values are computed according to the time offset of your web client.")
    tz_offset = fields.Char(compute='_compute_tz_offset', string='Timezone offset', invisible=True)

    @api.constrains('start_time')
    def _check_validity_start_time(self):
        """ verifies if start_time is earlier than end_time. """
        for attendance in self:
            if attendance.start_time >= 24:
                raise exceptions.ValidationError(_('"Start Time" time cannot be bigger than 23:59.'))

    @api.constrains('end_time')
    def _check_validity_end_time(self):
        """ verifies if end_time is earlier than end_time. """
        for attendance in self:
            if attendance.end_time >= 24:
                raise exceptions.ValidationError(_('"End Time" time cannot be bigger than 23:59.'))

    @api.constrains('start_time', 'end_time')
    def _check_validity_start_time_end_time(self):
        """ verifies if start_time is earlier than end_time. """
        for attendance in self:
            if attendance.start_time and attendance.end_time:
                if attendance.end_time < attendance.start_time:
                    raise exceptions.ValidationError(_('"End Time" time cannot be earlier than "Start Time".'))

    @api.depends('tz')
    def _compute_tz_offset(self):
        for partner in self:
            partner.tz_offset = datetime.datetime.now(pytz.timezone(partner.tz or 'GMT')).strftime('%z')

    def attendance_check_rule(self, last_check_in, now):
        tipe = 'in' if not last_check_in else 'out'
        rule_ids = self.env['sni.attendance.rule'].sudo().search([('attendance_type', '=', tipe)])
        if rule_ids:
            for rule_id in rule_ids:
                tz = pytz.timezone(rule_id.tz)
                _logger.info(_("TZ: {}".format(tz)))
                rule_time = pytz.utc.localize(now).astimezone(tz)
                _logger.info(_("rule_time: {}".format(rule_time)))
                t = rule_time.time()
                time = t.hour+t.minute/60.0
                _logger.info(_("time: {}".format(time)))
                _logger.info(_("start_time: {}".format(rule_id.start_time)))
                _logger.info(_("end_time: {}".format(rule_id.end_time)))

                if rule_id.start_time <= time and rule_id.end_time >= time:
                    return True

        return False

    attendance_type = fields.Selection([
        ('in', 'Check In'),
        ('out', 'Check Out')
    ], string="Attendance Type", required=True)

    description = fields.Text()