# -*- coding: utf-8 -*-

import pytz

from odoo import models, fields, api, _

from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

class Rfid(models.Model):
    _inherit = 'sni.rfid'

    def check_attendance_rule(self, last_check_in, now):
        attendance_rule = self.env['sni.attendance.rule'].sudo().search([])
        t = now.time()
        time = t.hour+t.minute/60.0

        if attendance_rule:
            granted = True
            message = False
            if not last_check_in:
                message = 'to Check In'
            elif last_check_in:
                message = 'to Check Out'

            granted = self.env['sni.attendance.rule'].sudo().attendance_check_rule(last_check_in, now)
            if not granted:
                return {'status': 'error', 'message': 'Wrong time {}, try again at the right time'.format(message), 'data': last_check_in._to_json() if last_check_in else {'utc': now.strftime('%Y-%m-%d %H:%M:%S')}}

        return False


    def attendance_action(self, new_rfid = False):
        if new_rfid:
            return {'status': 'success', 'message': 'Nice to meet you, {}. Please contact your admin to verify'.format(self.name), 'data': {}}

        if not self.is_verified:
            return {'status': 'success', 'message': 'Hello, {}. You are unverified. Please contact your admin to verify'.format(self.name)}

        domain = [
            ('rfid_id', '=', self.id),
            ('check_out', '=', False)
        ]
        last_check_in = self.env['sni.attendance'].search(domain)
        
        if len(last_check_in) > 1:
            _logger.info('[WARNING] RFID Attendance Overlaping : {}'.format(self.name))

        now = fields.Datetime.now()

        # Check Rule
        check_attendance_rule = self.check_attendance_rule(last_check_in, now)

        if check_attendance_rule:
            return check_attendance_rule

        if not last_check_in:
            _logger.info('[ATTENDANCE] Check In : {}'.format(self.name))
            last_check_in = self.env['sni.attendance'].create({
                'rfid_id': self.id,
                'check_in': fields.Datetime.now()
            })
            return {'status': 'success', 'message': 'Hello, {}'.format(self.name), 'data': last_check_in._to_json()}
        else:
            _logger.info('[ATTENDANCE] Check Out : {}'.format(self.name))
            last_check_in.write({
                'check_out': fields.Datetime.now(),
            })

            return {'status': 'success', 'message': 'Goodbye, {}'.format(self.name), 'data': last_check_in._to_json()}