# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class User(models.Model):
    _inherit = ['res.users']

    rfid_id = fields.Many2one('sni.rfid', 'RFID')