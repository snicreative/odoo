# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions, _
from odoo.tools import format_datetime

class Attendance(models.Model):
    _name = 'sni.attendance'
    _description = 'Attendance with RFID'
    _order = "check_in desc"

    def _default_rfid(self):
        return self.env.user.rfid_id or False

    rfid_id = fields.Many2one('sni.rfid', string="RFID", default=_default_rfid, required=True, ondelete='cascade', index=True)

    check_in = fields.Datetime(string="Check In", default=fields.Datetime.now, required=True)
    check_out = fields.Datetime(string="Check Out")
    worked_hours = fields.Float(string='Hours', compute='_compute_worked_hours', store=True, readonly=True)

    def name_get(self):
        result = []
        for attendance in self:
            if not attendance.check_out:
                result.append((attendance.id, _("%(rfid_key)s from %(check_in)s") % {
                    'rfid_key': attendance.rfid_id.name,
                    'check_in': format_datetime(self.env, attendance.check_in, dt_format=False),
                }))
            else:
                result.append((attendance.id, _("%(rfid_key)s from %(check_in)s to %(check_out)s") % {
                    'rfid_key': attendance.rfid_id.name,
                    'check_in': format_datetime(self.env, attendance.check_in, dt_format=False),
                    'check_out': format_datetime(self.env, attendance.check_out, dt_format=False),
                }))
        return result

    @api.depends('check_in', 'check_out')
    def _compute_worked_hours(self):
        for attendance in self:
            if attendance.check_out and attendance.check_in:
                delta = attendance.check_out - attendance.check_in
                attendance.worked_hours = delta.total_seconds() / 3600.0
            else:
                attendance.worked_hours = False

    @api.constrains('check_in', 'check_out')
    def _check_validity_check_in_check_out(self):
        """ verifies if check_in is earlier than check_out. """
        for attendance in self:
            if attendance.check_in and attendance.check_out:
                if attendance.check_out < attendance.check_in:
                    raise exceptions.ValidationError(_('"Check Out" time cannot be earlier than "Check In" time.'))

    def _to_json(self):
        self.ensure_one()

        res = {
            'check_in': self.check_in.strftime("%Y-%m-%d %H:%M:%S"),
            'check_out': self.check_out.strftime("%Y-%m-%d %H:%M:%S") if self.check_out else '',
        }

        if self.rfid_id:
            res['rfid_id'] = self.rfid_id._to_json()

        return res