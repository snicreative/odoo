# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class Attendance(models.Model):
    _inherit = 'sni.attendance'

    temperature_in = fields.Float('Temperature Check In')
    temperature_out = fields.Float('Temperature Check Out')