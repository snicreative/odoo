# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

class Rfid(models.Model):
    _inherit = 'sni.rfid'

    def attendance_action(self, new_rfid = False, temperature=0):
        if new_rfid:
            return {'status': 'success', 'message': 'Nice to meet you, {}. Please contact your admin to verify'.format(self.name), 'data': {}}

        if not self.is_verified:
            return {'status': 'success', 'message': 'Hello, {}. You are unverified. Please contact your admin to verify'.format(self.name), 'data': {}}

        domain = [
            ('rfid_id', '=', self.id),
            ('check_out', '=', False)
        ]
        last_check_in = self.env['sni.attendance'].search(domain)

        if len(last_check_in) > 1:
            _logger.info('[WARNING] RFID Attendance Overlaping : {}'.format(self.name))

        now = fields.Datetime.now()

        # Check Rule
        check_attendance_rule = self.check_attendance_rule(last_check_in, now)

        if check_attendance_rule:
            return check_attendance_rule

        if not last_check_in:
            _logger.info('[ATTENDANCE] Check In : {}'.format(self.name))
            last_check_in = self.env['sni.attendance'].create({
                'rfid_id': self.id,
                'check_in': now,
                'temperature_in': temperature,
            })

            if self.orang_id:
                if self.orang_id.tipe_orang == 'siswa':
                    if self.orang_id.orangtua_id:
                        self.orang_id.orangtua_id.send_whatsapp("Siswa {} masuk pada pukul {}.".format(self.orang_id.name, now.strftime('%I:%M %p')))
            return {'status': 'success', 'message': 'Hello, {}'.format(self.name), 'data': last_check_in._to_json()}
        else:
            _logger.info('[ATTENDANCE] Check Out : {}'.format(self.name))
            last_check_in.write({
                'check_out': now,
                'temperature_out': temperature,
            })

            if self.orang_id:
                if self.orang_id.tipe_orang == 'siswa':
                    if self.orang_id.orangtua_id:
                        self.orang_id.orangtua_id.send_whatsapp("Siswa {} pulang pada pukul {}.".format(self.orang_id.name, now.strftime('%I:%M %p')))

            return {'status': 'success', 'message': 'Goodbye, {}'.format(self.name), 'data': last_check_in._to_json()}
