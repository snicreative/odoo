# -*- coding: utf-8 -*-

{
    'name': 'SNI SMA Negeri 12 Bandung',
    'version': '1.0.0',
    'category': 'School Management System',
    'author': 'SNI',
    'summary': 'SNI School Management System',
    'sequence': 10,
    'description': """
    SNI School Management System for SMA Negeri 12 Bandung
    """,
    'depends': [
        'sni_core',
        'sni_sms',
        'sni_attendance',
        'sni_rfid',
        'sni_guestbook',
        'sni_accounting',
        'ks_curved_backend_theme',
    ],
    'data': [
        # security

        # views
        'views/sni_attendance_views.xml',
    ],
    'application': False
}