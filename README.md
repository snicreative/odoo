# SNI Odoo Addons Installation

## Requirement
1. Ubuntu 18.04 or later
2. Python 3.7 or later
3. PostgreSQL 10.0 and later
4. Odoo 14.0

## Installation

1. Log in to your VPS via SSH
```bash
ssh user@VPS_ip
```
2. Update the system and install all necessary packages
```bash
sudo apt-get update && apt-get -y upgrade
sudo apt-get install git wkhtmltopdf python3-pip python-dev \
    python-virtualenv virtualenv libevent-dev gcc libjpeg-dev libxml2-dev \
    libssl-dev libsasl2-dev node-less libldap2-dev libxslt-dev
```
3. Install PostgreSQL
```bash
apt install postgresql postgresql-server-dev
systemctl enable postgresql.service
systemctl start postgresql.service
```
4. Create Odoo User

Install user for System
```bash
sudo adduser --system --group odoo --home /opt/odoo
```
Install user for PostgreSQL
```bash
su - postgres -c "createuser --createdb --username postgres --no-createrole --no-superuser --no-password odoo"
```

5. Install Odoo

First, we will install Odoo version 14. To switch to user odoo run:
```bash
sudo su - odoo -s /bin/bash
```

Clone the Odoo 14.0 branch to `server` directory from github:
```bash
git clone https://www.github.com/odoo/odoo.git --depth 1 --single-branch --branch 14.0 server
```

Create python virtual environment and install all requirements:
```bash
cd /opt/odoo
virtualenv ./venv
source ./venv/bin/activate
pip install -r server/requirements.txt
```

Create `addons` directory and **Clone Odoo SNI Project**
```bash
mkdir addons
cd addons
git clone https://gitlab.com/snicreative/odoo.git snicreative
git clone https://gitlab.com/snicreative/asset/odoo-addons.git premium-addons
```

Switch back your user:
```bash
exit
```

6. Configure Odoo
```bash
sudo nano /etc/odoo.conf
```

```
[options]
admin_passwd = your_strong_admin_password
db_host = False
db_port = False
db_user = odoo
db_password = False
addons_path = /opt/odoo/server/addons, /opt/odoo/addons, /opt/odoo/addons/snicreative, /opt/odoo/addons/premium-addons
logfile = /var/log/odoo.log
xmlrpc_port = 8069
```

Create SystemD scripts:

```bash
sudo nano /lib/systemd/system/odoo.service
```

```
[Unit]
Description=Odoo Server 14.0 
Requires=postgresql.service
After=postgresql.service

[Service]
Type=simple
PermissionsStartOnly=true
User=odoo
Group=odoo
SyslogIdentifier=odoo
ExecStart=/opt/odoo/venv/bin/python /opt/odoo/server/odoo-bin -c /etc/odoo.conf

[Install]
WantedBy=multi-user.target
```

Create Log File
```bash
touch /var/log/odoo.log
chown odoo:odoo /var/log/odoo.log
```

7. Final Steps

Enable and Start services
```bash
sudo systemctl enable odoo.service
sudo systemctl start odoo.service
```